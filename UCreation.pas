Unit UCreation;

Interface
Uses UTypePolynome;


FUNCTION GenPolynome ():Polynome;
FUNCTION GenMonome (coeff : real;degre:integer):Polynome;
FUNCTION SuppMonome (Poly :Polynome):Polynome;
implementation

(*--------------------------------------------------------------
 --nom            : InsertDeb
 --rôle           : Insert un maillon au début d'une chaîne de pointeurs
 --pré-conditions : une liste initiale de pointeurs, un nouveau maillon de types PtrMaillon
 --résultat       : un pointeur de type PtrMaillon
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION InsertDeb (l,nv :Polynome): Polynome;
BEGIN
   nv^.suivant:=l;
   InsertDeb:=nv;
   Writeln('InsertDebOK');
END; { InsertDeb }

(*--------------------------------------------------------------
 --nom            : InsertFin
 --rôle           : Insert un maillon à la fin d'une chaîne
 --pré-conditions : une liste initiale de pointeurs, un nouveau maillon de types PtrMaillon
 --résultat       : un pointeur de type PtrMaillon
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION InsertFin (l,nv :Polynome ):Polynome;
VAR
   lpremier :Polynome;
BEGIN
   lpremier:=l;
   while (l^.suivant^.suivant<>nil)do
   Begin
      l:=l^.suivant;
   End;
   nv^.suivant:=l^.suivant;
   l^.suivant:=nv;
   InsertFin:=lpremier;
   Writeln('InsertFin OK');
END; { InsertFin }

(*--------------------------------------------------------------
 --nom            : SuppMonome
 --rôle           : Supprime les monômes nuls d'un polynôme
 --pré-conditions : Un polynôme (pointeur initial d'une liste chaînée)
 --résultat       : Un polynôme sans coefficient nuls (pointeur initial d'une liste chaînée)
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION SuppMonome (Poly :Polynome):Polynome;
   Var
      PolyPremier,PolySuppr : Polynome;
BEGIN
   While(Poly^.mono.coeff=0)do
   Begin
      PolySuppr:=Poly;
      Poly:=Poly^.suivant;
      dispose(PolySuppr);
   End;
   PolyPremier:=Poly;
   Repeat
      If(Poly^.suivant^.mono.coeff=0)then
      Begin
         PolySuppr:=Poly^.suivant;
         Poly^.suivant:=Poly^.suivant^.suivant;
         dispose(PolySuppr);
      End;
      Poly:=Poly^.suivant;
   Until(Poly^.suivant=nil);
   SuppMonome:=PolyPremier;
   Writeln('supp ok');
END; { SuppMonome }




(*--------------------------------------------------------------
 --nom            : SumMonome
 --rôle           : Retourne la somme de deux monômes consécutifs de même degré
 --pré-conditions : Un polynome (pointeur initial d'une liste chaînée de monômes)
 --résultat       : Un polynôme (pointeur initial d'une liste chaînée de monômes)
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION SumMonome (Poly :Polynome): Polynome;
BEGIN
   New(SumMonome);
   If(Poly^.mono.coeff=0)then
      Begin
         SumMonome^.mono.coeff:=Poly^.suivant^.mono.coeff;
         SumMonome^.mono.deg:=Poly^.mono.deg;
      End
   Else
      If(Poly^.suivant^.mono.coeff=0)then
      Begin
         SumMonome^.mono.coeff:=Poly^.mono.coeff;
         SumMonome^.mono.deg:=Poly^.mono.deg;
         End
   Else
      Begin
         SumMonome^.mono.coeff:=Poly^.mono.coeff+Poly^.suivant^.mono.coeff;
         SumMonome^.mono.deg:=Poly^.mono.deg;
      End;
END; { SumMonome }

(*--------------------------------------------------------------
 --nom            : Verifdeg
 --rôle           : Vérifie si il n'y a pas d'égalité de degrés dans les monômes, si oui additionne les coefficients 
 --pré-conditions : Un Polynôme ( un pointeur initial d'une liste chaînée
 --résultat       : Un polynôme (un pointeur initial d'une liste chaînée)
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION Verifdeg (Poly : Polynome ):Polynome;
Var
   PolyPremier,PolyIter : polynome;
BEGIN
   PolyPremier:=Poly;
   While(Poly^.suivant^.suivant^.suivant<>nil)do
      Begin
         While(Poly^.mono.deg=Poly^.suivant^.mono.deg)and(Poly^.suivant<>nil)do
         Begin
            Poly:=SumMonome(Poly);
            PolyIter:=Poly^.suivant;
            Poly^.suivant:=Poly^.suivant^.suivant;
            Dispose(PolyIter);
         End;
         Poly:=Poly^.suivant;
      End;
   Verifdeg:=PolyPremier;
   writeln('VerifDeg OK');
END; { Verifdeg }




(*--------------------------------------------------------------
 --nom            : AjoutPos
 --rôle           : Ajoute un monome à une position donnée dans un polynôme (pointeur initial liste chaînée)
 --pré-conditions : Un monôme, et un polynome (pointeur initial liste chaînée), la position où insérer le monome (un entier)
 --résultat       : Le polynôme avec le monôme inséré à la position donnée
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION AjoutPos (Poly,nv : polynome;pos,i:integer):Polynome;
BEGIN
   If(nv^.mono.coeff=0)then
      Begin
         Poly:=InsertFin(Poly,nv);
      End
Else
Begin
   If(pos=i)then
   Begin
      Poly:=InsertDeb(Poly,nv);
   End
Else
Begin
   If((i<=pos-1)and (Poly<>nil))then
   Begin
      Poly^.suivant:=Ajoutpos(Poly^.suivant,nv,pos,i+1);
   End;
End;
End;
   AjoutPos:=Poly;
   Writeln('AjoutPosOK');
END; { AjoutPos }





(*--------------------------------------------------------------
 --nom            : RecherchePos
 --rôle           : Recherche la position d'un monôme dans un polynôme en fonction de son degré 
 --pré-conditions : Un polynôme (Pointeur initial liste chaînée), le degré du monôme en question
 --résultat       : La position du monôme dans le polynôme (un entier)
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION RecherchePos (Poly,nv : Polynome ):integer;
BEGIN
   If(nv^.mono.deg>=poly^.mono.deg)or (poly^.suivant=nil)then
   Begin
      RecherchePos:=1;
   End
   Else
   Begin
      RecherchePos:=1+RecherchePos(Poly^.suivant,nv);
   End;   
   Writeln('RecherchePos Ok');
END; { RecherchePos }



(*--------------------------------------------------------------
 --nom            : GenMonome
 --rôle           : Génère un monôme
 --pré-conditions :  aucunes
 --résultat       : un monôme ( record d'un entier et d'un réel)
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION GenMonome (coeff : real;degre:integer):Polynome;
BEGIN
   New(GenMonome);
   GenMonome^.mono.deg:=degre;
   GenMonome^.mono.coeff:=coeff;
   GenMonome^.suivant:=nil;
   Writeln('GenmonomOK');
END; { GenMonome }

(*--------------------------------------------------------------
 --nom            : GenPolynome
 --rôle           : Permet de générer un polynôme
 --pré-conditions :  aucunes
 --résultat       : Un polynome (pointeur initial d'une liste chaînée)
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION GenPolynome ():Polynome;
VAR
   val: donnee;
   res,PolyIter,PolyFin: Polynome;
BEGIN
   Writeln('Entrez le degré puis le coefficient des monômes constituant le polynôme un par un, lorsque vous aurez terminé, entrez le degré puis le coefficient nul');
   New(res);
   New(PolyFin);
   Read(val.deg);
   Read(val.coeff);
   Res:=GenMonome(val.coeff,val.deg);
   PolyFin^.mono.deg:=0;
   PolyFin^.mono.coeff:=0;
   Polyfin^.suivant:=nil;
   Res^.suivant:=PolyFin;
   While(val.coeff<>0)do
     Begin
        new(PolyIter);
        Read(val.deg);
        Read(val.coeff);
        PolyIter:=GenMonome(val.coeff,val.deg);
        Res:=AjoutPos(res,polyIter,RecherchePos(res,PolyIter),1);
     End; 
   GenPolynome:=SuppMonome(res);
   Writeln('GenPolynome ok');
END; { GenPolynome }

Begin
End.
