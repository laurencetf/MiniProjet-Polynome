Unit Uaffichage;

Interface
Uses UTypePolynome;

PROCEDURE DispPolynome (Poly :Polynome);

Implementation

(*--------------------------------------------------------------
 --nom            : DispPositif
 --rôle           : Affiche un monome positif
 --pré-conditions : un polynome (pointeur initial d'une liste chaînée)
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
PROCEDURE DispPositif (Poly :Polynome );
BEGIN
   If((Poly^.mono.coeff<>0)and(Poly^.mono.deg=1))then
   Begin
      Write('+',round(Poly^.mono.coeff),'X');
   End
else
   If ((Poly^.mono.coeff<>0)and(Poly^.mono.deg<>0))then
   Begin
      Write('+',round(Poly^.mono.coeff),'X^',Poly^.mono.deg);         
   end
else
   If((Poly^.mono.coeff<>0)and(Poly^.mono.deg=0))then
      Begin
         Write('+',round(Poly^.mono.coeff));
      End;
END; { DispPositif }

(*--------------------------------------------------------------
 --nom            : DispNegatif
 --rôle           : Affiche un monome Negatif
 --pré-conditions : un polynome (pointeur initial d'une liste chaînée)
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
PROCEDURE DispNegatif (Poly :Polynome );
BEGIN
   If((Poly^.mono.coeff<>0)and(Poly^.mono.deg=1))then
   Begin
      Write(round(Poly^.mono.coeff),'X');
   End
else
   Begin
         If ((Poly^.mono.coeff<>0)and(Poly^.mono.deg<>0))then
         Begin
            Write(round(Poly^.mono.coeff),'X^',Poly^.mono.deg);         
         End
      Else
            If((Poly^.mono.coeff<>0)and(Poly^.mono.deg=0))then
            Begin
               Write('+',round(Poly^.mono.coeff));
            End;
   End;
END; { DispNegatif}


(*--------------------------------------------------------------
 --nom            : DispPolynome
 --rôle           : Affiche un polynôme
 --pré-conditions : Un polynome (le pointeur initial d'une liste chaînée)
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
PROCEDURE DispPolynome (Poly : Polynome);
BEGIN
   Write(Round(Poly^.mono.coeff),'X^',Poly^.mono.deg);
   Poly:=Poly^.suivant;
   While(Poly^.suivant<>nil)do
   Begin
      If(Poly^.mono.coeff>=0)then
      Begin
         DispPositif(Poly);
      end
      Else
         Begin
            DispNegatif(Poly);
         End;
      Poly:=Poly^.suivant;
   End;
      If(Poly^.mono.coeff>=0)then
      Begin
         DispPositif(Poly);
      end
      Else
         Begin
            DispNegatif(Poly);
         End;
   Writeln(' ');
END; { DispPolynome } 

Begin
End.
