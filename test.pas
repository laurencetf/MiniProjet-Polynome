
(*
 -------------------------------------------------------------------
 Nom Programme    :  test.pas
 Auteur           :  Laurencet Fabien <laurencetf@eisti.eu>
 Description      :  <description>
 Date de création :  Fri Mar 25 19:57:31 2016
 Compilation      :  fpc
 Execution        :  shell
 -------------------------------------------------------------------
 *)

PROGRAM  test;
USES crt,UCreation,UTypePolynome,UAffichage,UDerivation;

Var
   Poly1 : POlynome;
BEGIN
   Poly1:=GenPolynome();
   DispPolynome(Poly1);
   Poly1:=CalculDerivee(Poly1);
   DispPolynome(Poly1); 
END.
