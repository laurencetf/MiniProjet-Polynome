Unit UDerivation;

Interface
Uses UTypePolynome,UCreation;
FUNCTION CalculDerivee (Poly : Polynome ):Polynome;
FUNCTION Reverse (Poly : Polynome):Polynome;
Implementation


(*--------------------------------------------------------------
 --nom            : Reverse
 --rôle           : Retourne une liste chaînée inversée
 --pré-conditions : Un polynôme (pointeur initial d'une liste chaînée de monômes)
 --résultat       : Un polynôme inversé (pointeur initial d'une liste chaînée de monômes)
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION Reverse (Poly : Polynome):Polynome;
Var
   Q : Polynome;
BEGIN
   If((Poly^.suivant^.mono.coeff=0)and(Poly^.suivant^.mono.deg=0))then
   Begin
      New(Reverse);
      Reverse:=Poly;
   End
Else
Begin
   New(Q);
   New(Reverse);
   Reverse:=Reverse(Poly^.suivant);
   Q:=Poly^.suivant;
   Q^.suivant:=Poly;
   Poly^.suivant:=nil;
End;
End; { Reverse }
         



   
(*--------------------------------------------------------------
 --nom            : CalculDerivee
 --rôle           : Permet de calculer la dérivée d'un polynôme
 --pré-conditions : un polynôme (polynome)
 --résultat       : un polynôme (polynome)
 --auteur         :  Fabien Laurencet <laurencetf@eisti.eu>
 ----------------------------------------------------------------
 *)
FUNCTION CalculDerivee (Poly : Polynome ):Polynome;
Var
   Res,Polyfin,PolyIter : Polynome;
BEGIN
   New(res);
   res:=GenMonome((round(Poly^.mono.coeff*Poly^.mono.deg)),Poly^.mono.deg-1);
   Polyfin:=GenMonome(0,0);
   res^.suivant:=PolyFin;
   While((Poly^.mono.deg<>0)and(Poly^.mono.coeff<>0))do
      Begin
         Poly:=Poly^.suivant;
         If(Poly^.mono.deg>1)then
            Begin
               PolyIter:=GenMonome((round(Poly^.mono.coeff*Poly^.mono.deg)),(Poly^.mono.deg-1));
            End
         Else
            Begin
            PolyIter:=GenMonome(0,1);
            End;
         PolyIter^.suivant:=res;
         res:=PolyIter;
      End;
   CalculDerivee:=Reverse(res);
   Writeln('ok');
END; { CalculDerivee }

Begin
End.
