Unit UTypePolynome;

Interface
Type
   {Polynôme ( liste chaînée de monômes) }
   Polynome  = ^monome;
   
   {Contient le degré et le coefficient d'un monôme }
   Donnee = record
               deg   : integer;
               coeff : real;
            end;
   
   {Monôme les informations du monôme, le suivant sur la liste}
   Monome=record
             mono      : Donnee;
             suivant   : Polynome;
          end;

Implementation

Begin
End.
