(*
 ___________________________________________________________________
|
|=============  Nom:			Addition
|=============  Date:			22 Mars 2016
|=============  Auteur:			Rémi Lacroix <lacroixrem@eisti.eu>
|___________________________________________________________________
*)

PROGRAM Addition;

Type
   {Polynôme ( liste chaînée de monômes) }
   Polynome  = ^monome;
   {Contient le degré et le coefficient d'un monôme }
   Donnee = record
               deg   : integer;
               coeff : real;
            end;     
   {Monôme les informations du monôme, le suivant sur la liste}
   Monome=record
             mono    : Donnee;
             suivant : Polynome;
          end;
   {Record pour la postion du Monome } 
   Position=  record
                 pos : integer;
                 fin : boolean;
              end;


(*
 _______________________________________________________________________________________
|
|=============  Nom:			 Additionner
|=============  Date:			 22 Marq 2016
|=============  Auteur:			 Rémi Lacroix <lacroixrem@eisti.eu>
|=============  Utilité:		 Additionner deux polynomes
|=============  Pré-Conditions:	
|				*	P : Polynôme (Pointeur vers le premier maillon de la liste)
|				*	Q : Polynôme (Pointeur vers le premier maillon de la liste)
|=============  Post-Conditions: Polynôme (Pointeur vers le premier maillon de la liste)
|_______________________________________________________________________________________
*)

Function Additionner(P,Q:Polynome):Polynome;
Var
	a,b : Donnee;
	ad : Polynome;
Begin
	new(ad);
	a:=P^.mono;
	b:=Q^.mono;
	If (P^.suivant<>Nil) and (Q^.suivant<>Nil) Then
	Begin
		If (a.deg=b.deg) Then
		Begin
			ad^.mono.deg:=a.deg;
			ad^.mono.coeff:=a.coeff+b.coeff;
			new(ad^.suivant);
			ad^.suivant:=Additionner(P^.suivant,Q^.suivant);
		End
		Else
		Begin
			If (a.deg>b.deg) Then
			Begin
				ad^.mono.deg:=a.deg;
				ad^.mono.coeff:=a.coeff;
				new(ad^.suivant);
				ad^.suivant:=Additionner(P^.suivant,Q);
			End
			Else
			Begin
				ad^.mono.deg:=b.deg;
				ad^.mono.coeff:=b.coeff;
				new(ad^.suivant);
				ad^.suivant:=Additionner(P,Q^.suivant);
			End;
		End;
	End;
	Additionner:=ad;
End; {Additionner}



(*
 _______________________________________________________________________________________
|
|=============  Nom:			 Soustraire
|=============  Date:			 22 Mars 2016
|=============  Auteur:			 Rémi Lacroix <lacroixrem@eisti.eu>
|=============  Utilité:		 Soustraire deux polynomes
|=============  Pré-Conditions:	
|				*	P : Polynôme (Pointeur vers le premier maillon de la liste)
|				*	Q : Polynôme (Pointeur vers le premier maillon de la liste)
|=============  Post-Conditions: Polynôme (Pointeur vers le premier maillon de la liste)
|_______________________________________________________________________________________
*)

Function Soustraire(P,Q:Polynome):Polynome;
Var
	a,b : Donnee;
	so : Polynome;
Begin
	new(so);
	a:=P^.mono;
	b:=Q^.mono;
	If (P^.suivant<>Nil) and (Q^.suivant<>Nil) Then
	Begin
		If (a.deg=b.deg) Then
		Begin
			so^.mono.deg:=a.deg;
			so^.mono.coeff:=a.coeff-b.coeff;
			new(so^.suivant);
			so^.suivant:=Soustraire(P^.suivant,Q^.suivant);
		End
		Else
		Begin
			If (a.deg>b.deg) Then
			Begin
				so^.mono.deg:=a.deg;
				so^.mono.coeff:=a.coeff;
				new(so^.suivant);
				so^.suivant:=Soustraire(P^.suivant,Q);
			End
			Else
			Begin
				so^.mono.deg:=b.deg;
				so^.mono.coeff:=-b.coeff;
				new(so^.suivant);
				so^.suivant:=Soustraire(P,Q^.suivant);
			End;
		End;
	End;
	Soustraire:=so;
End; {Soustraire}


(*
 _______________________________________________________________________________________
|
|=============  Nom:			 MultiMonomes
|=============  Date:			 25 Mars 2016
|=============  Auteur:			 Rémi Lacroix <lacroixrem@eisti.eu>
|=============  Utilité:		 Multiplier deux monômes
|=============  Pré-Conditions:	
|				*	M1
|				*	M2
|				*	
|=============  Post-Conditions: 
|_______________________________________________________________________________________
*)

Function MultiMonomes(M1:Donnee;M2:Donnee):Donnee;
Begin
	MultiMonomes.coeff:=M1.coeff*M2.coeff;
	MultiMonomes.deg:=M1.deg+M2.deg;
End; {MultiMonomes}


(*
 _______________________________________________________________________________________
|
|=============  Nom:			 MultiMonoPoly
|=============  Date:			 25 Mars 2016
|=============  Auteur:			 Rémi Lacroix <lacroixrem@eisti.eu>
|=============  Utilité:		 Multiplier un monôme et un polynôme
|=============  Pré-Conditions:	
|				*	M
|				*	P
|				*	
|=============  Post-Conditions: 
|_______________________________________________________________________________________
*)

Function MultiMonoPoly(M:Donnee;P:Polynome):Polynome;
Var
	Ptmp : Polynome;
Begin
	Ptmp:=P;
	If (Ptmp<>Nil) Then
	Begin
		Ptmp^.mono:=MultiMonomes(M,Ptmp^.mono);
		Ptmp^.suivant:=MultiMonoPoly(M,Ptmp^.suivant);
	End;
	MultiMonoPoly:=Ptmp;
End; {MultiMonoPoly}



(*
 _______________________________________________________________________________________
|
|=============  Nom:			 Multiplication
|=============  Date:			 23 Mars 2016
|=============  Auteur:			 Rémi Lacroix <lacroixrem@eisti.eu>
|=============  Utilité:		 Multiplier deux polynômes
|=============  Pré-Conditions:	
|				*	P
|				*	Q
|				*	
|=============  Post-Conditions: 
|_______________________________________________________________________________________
*)

Function Multiplication(P,Q:Polynome):Polynome;
Var
	Ptmp : Polynome;
Begin
	Ptmp:=P;
	If (Ptmp<>Nil) and (Q<>Nil) Then
	Begin
		Ptmp:=MultiMonoPoly(Ptmp^.mono,Q);
		Ptmp^.suivant:=Multiplication(Ptmp^.suivant,Q);
	End;
	Writeln('MultiplicationOk');
	Multiplication:=P;
End; {Multiplication}





FUNCTION InsertDeb (l,nv :Polynome): Polynome;
BEGIN
   nv^.suivant:=l;
   InsertDeb:=nv;
END; { InsertDeb }


FUNCTION Verifdeg (Poly : Polynome ):Polynome;
Var
   POlyPremier,PolyIter : polynome;
BEGIN
   PolyPremier:=Poly;
   While(Poly^.suivant<>nil)do
      Begin
      While(Poly^.mono.deg=Poly^.suivant^.mono.deg)do
      Begin
         Poly^.mono.coeff:=Poly^.mono.coeff+Poly^.suivant^.mono.coeff;
         PolyIter:=Poly^.suivant;
         Poly^.suivant:=Poly^.suivant^.suivant;
         Dispose(PolyIter);
      End;
      Poly:=Poly^.suivant;
   End;
   Verifdeg:=PolyPremier;
END; { Verifdeg }



FUNCTION AjoutPos (Poly,nv : polynome;pos:integer):Polynome;
BEGIN
   If(pos=1)then
   Begin
      Poly:=InsertDeb(Poly,nv);
   End
   Else
   Begin
      If((1<=pos-1)and (Poly<>nil))then
      Begin
         Poly^.suivant:=Ajoutpos(Poly^.suivant,nv,pos-1);
      End;
   End;
   AjoutPos:=Poly;
END; { AjoutPos }


FUNCTION RecherchePos (Poly : Polynome; deg : integer ):integer;
BEGIN
   If(deg>=poly^.mono.deg)or (poly^.suivant=nil)then
   Begin
      RecherchePos:=1;
   End
   Else
   Begin
      RecherchePos:=1+RecherchePos(Poly^.suivant,deg);
   End;   
END; { RecherchePos }



FUNCTION GenMonome (coeff : real;degre:integer):Polynome;
BEGIN
   New(GenMonome);
   GenMonome^.mono.deg:=degre;
   GenMonome^.mono.coeff:=coeff;
   GenMonome^.suivant:=nil;
END; { GenMonome }




FUNCTION GenPolynome ():Polynome;
VAR
   Coeff        : real;
   deg,posi         : integer;
   res,PolyIter,PolyFin: Polynome;
BEGIN
   Writeln('Entrez le degré puis le coefficient des monômes constituant le polynôme un par un, lorsque vous aurez terminé, entrez un coefficient nul');
   New(res);
   New(PolyFin);
   Read(deg);
   Read(coeff);
   Res:=GenMonome(coeff,deg);
   PolyFin^.mono.deg:=0;
   PolyFin^.mono.coeff:=0;
   Polyfin^.suivant:=nil;
   Res^.suivant:=PolyFin;
  While(coeff<>0)do
     Begin
        new(PolyIter);
        Read(deg);
        Read(coeff);
        PolyIter:=GenMonome(coeff,deg);
        posi:=RecherchePos(res,PolyIter^.mono.deg);
        Res:=AjoutPos(res,polyIter,posi);
     End; 
   GenPolynome:=Verifdeg(res);
END; { GenPolynome }


PROCEDURE DispPolynome (Poly :Polynome);
BEGIN
   While(Poly^.suivant^.suivant^.suivant<>nil)do
   Begin
      If ((Poly^.mono.coeff<>0)and(Poly^.mono.deg<>0))then
      Begin
         Write(trunc(Poly^.mono.coeff),'X^',Poly^.mono.deg,'+');         
      end;
      If((Poly^.mono.coeff<>0)and(Poly^.mono.deg=0))then
      Begin
         Write(trunc(Poly^.mono.coeff),'+');
      end;
      If((Poly^.mono.coeff<>0)and(Poly^.mono.deg=1))then
      Begin
         Write(trunc(Poly^.mono.coeff),'X+');
      end;
      Poly:=Poly^.suivant;
   End;
   Writeln(trunc(Poly^.mono.coeff),'X^',Poly^.mono.deg);
END; { DispPolynome }

Var
   P,Q,A,S,M : Polynome;
Begin
	new(A);
	new(S);
	new(M);
	P:=GenPolynome();
	Write('P : ');
	DispPolynome(P);
	Q:=GenPolynome();
	Write('Q : ');
	DispPolynome(Q);
	A:=Additionner(P,Q);
	Write('P+Q : ');
	DispPolynome(A);
	S:=Soustraire(P,Q);
	Write('P-Q : ');
	DispPolynome(S);
	M:=Multiplication(P,Q);
	Write('P*Q : ');
	DispPolynome(M);
End.
