Program Multifruit;


Type
   {Polynôme ( liste chaînée de monômes) }
   Polynome  = ^monome;
   {Contient le degré et le coefficient d'un monôme }
   Donnee = record
               deg   : integer;
               coeff : real;
            end;     
   {Monôme les informations du monôme, le suivant sur la liste}
   Monome=record
             mono    : Donnee;
             suivant : Polynome;
          end;
   {Record pour la postion du Monome } 
   Position=  record
                 pos : integer;
                 fin : boolean;
              end;



(*
 _______________________________________________________________________________________
|
|=============  Nom:			 MultiMonomes
|=============  Date:			 25 Mars 2016
|=============  Auteur:			 Rémi Lacroix <lacroixrem@eisti.eu>
|=============  Utilité:		 Multiplier deux monômes
|=============  Pré-Conditions:	
|				*	M1
|				*	M2
|				*	
|=============  Post-Conditions: 
|_______________________________________________________________________________________
*)

Function MultiMonomes(M1:Donnee;M2:Donnee):Donnee;
Begin
	MultiMonomes.coeff:=M1.coeff*M2.coeff;
	MultiMonomes.deg:=M1.deg+M2.deg;
End; {MultiMonomes}

(*
 _______________________________________________________________________________________
|
|=============  Nom:			 MultiMonoPoly
|=============  Date:			 25 Mars 2016
|=============  Auteur:			 Rémi Lacroix <lacroixrem@eisti.eu>
|=============  Utilité:		 Multiplier un monôme et un polynôme
|=============  Pré-Conditions:	
|				*	M
|				*	P
|				*	
|=============  Post-Conditions: 
|_______________________________________________________________________________________
*)

Function MultiMonoPoly(M:Donnee;P:Polynome):Polynome;
Var
	Ptmp : Polynome;
Begin
	Ptmp:=P;
	If (Ptmp<>Nil) Then
	Begin
		Ptmp^.mono:=MultiMonomes(M,Ptmp);
		Ptmp^.suivant:=MultiMonoPoly(M,Ptmp^.suivant);
	End;
	MultiMonoPoly:=P;
End; {MultiMonoPoly}


(*
 _______________________________________________________________________________________
|
|=============  Nom:			 Multiplication
|=============  Date:			 23 Mars 2016
|=============  Auteur:			 Rémi Lacroix <lacroixrem@eisti.eu>
|=============  Utilité:		 Multiplier deux polynômes
|=============  Pré-Conditions:	
|				*	P
|				*	Q
|				*	
|=============  Post-Conditions: 
|_______________________________________________________________________________________
*)

Function Multiplication(P,Q:Polynome):Polynome;
Var
	a,b : Donnee;
	k : integer;
	Ptmp : Polynome;
Begin
	Ptmp:=P;
	If (Ptmp<>Nil) Then
	Begin
		Ptmp:=MultiMonoPoly(Ptmp^.mono,Q);
		Ptmp^.suivant:=Multiplication(Ptmp^.suivant,Q);
	End;
	Multiplication:=P;
End; {Multiplication}






Begin

End.